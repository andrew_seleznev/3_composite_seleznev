package by.seleznev.composite.enumeration;

public enum CompositeType {

	TEXT,
	LEAF,
	PARAGRAPH_WITH_LISTING("(([\\s]*)(\\t)(.[^\\t#]+))|((#)(.[^#]+)(#)([\\s[^\\t]]*))"), 
	PARAGRAPH("([\\s]*)(\\t)(.[^\\t#]+)"),
	SENTENCE("(\\t|\\s)?[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(\\s)+"), 
	SYMBOL(".{1}"),
	SPECIAL_SYMBOL("([^\\w']{1})"),
	LISTING("((#)(.[^#]+)(#)([\\s[^\\t]]*))"),
	WORD_AND_SIGN("([^\\w']{1})|([\\w']+)"),
	WORD("([\\w']+)");

	private String expression;

	private CompositeType(String expression) {
		this.expression = expression;
	}
	private CompositeType() {
		
	}
	public String getExpression() {
		return expression;
	}

}
