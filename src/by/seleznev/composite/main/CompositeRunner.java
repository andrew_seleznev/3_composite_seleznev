package by.seleznev.composite.main;

import java.nio.charset.Charset;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.seleznev.composite.action.CountComposite;
import by.seleznev.composite.action.SortComposite;
import by.seleznev.composite.entity.Composite;
import by.seleznev.composite.report.ReportComposite;
import by.seleznev.composite.service.CompositeFileService;
import by.seleznev.composite.service.CompositeParser;

public class CompositeRunner {

	static {
		new DOMConfigurator().doConfigure("conf/log4j.xml", LogManager.getLoggerRepository());
	}

	static Logger logger = Logger.getLogger(CompositeRunner.class);

	public static void main(String[] args) {

		String srcPath = "resourses/text.txt";
		String dstPath = "dst/text_new.txt";
		CompositeParser service = new CompositeParser();
		Composite composite = service.parseText(srcPath, Charset.forName("UTF-8"));
		String data = composite.buildText();
		CompositeFileService.writeTextFile(dstPath, data);

		SortComposite.sortWordsByConsAlph(composite);
		SortComposite.sortSentencesByWordAmount(composite);
		int count = CountComposite.counAllChartSymbols(composite);
		ReportComposite.printTotalNumberOfChars(count);
	}
}
