package by.seleznev.composite.report;

import java.util.ArrayList;
import java.util.List;

public class ReportComposite {

	public static void printSortedWordsByConsAlph(ArrayList<String> list) {
		System.out.println("========== Result of sorting words by consonant alphabet ==========");
		list.forEach(System.out::println);
	}

	public static void printSortedSentences(List<String> list) {
		System.out.println("==========  Result of sorting sentences by words amount  ==========");
		list.forEach(System.out::println);
	}
	
	public static void printTotalNumberOfChars(int count) {
		System.out.println("==========        Result of total chars counting         ==========");
		System.out.println("TOTAL: " + count);
	}

}
