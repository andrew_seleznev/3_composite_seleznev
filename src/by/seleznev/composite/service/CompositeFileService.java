package by.seleznev.composite.service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.log4j.Logger;

public class CompositeFileService {

	static Logger logger = Logger.getLogger(CompositeFileService.class);

	public static String readTextFile(String path, Charset encoding) {
		byte[] encoded = null;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
		} catch (IOException e) {
			logger.fatal("Requared .txt file hasn't been found");
			throw new RuntimeException(e.getMessage());
		}
		return new String(encoded, encoding);
	}

	public static void writeTextFile(String path, String data) {
		try {
			Files.write(Paths.get(path), data.getBytes());
		} catch (IOException e) {
			logger.error(e.getMessage());
		}
	}

}
