package by.seleznev.composite.service;

import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import by.seleznev.composite.entity.Composite;
import by.seleznev.composite.entity.Leaf;
import by.seleznev.composite.entity.Listing;
import by.seleznev.composite.enumeration.CompositeType;

public class CompositeParser {
	
	static Logger logger = Logger.getLogger(CompositeParser.class);

	public Composite parseText(String srcPath, Charset charset) {
		String text = CompositeFileService.readTextFile(srcPath, charset);
		Composite textConposite = new Composite(CompositeType.TEXT);
		textConposite = parseToParagraf(textConposite, text);
		logger.info("Component - text has been created");
		return textConposite;
	}

	private Composite parseToParagraf(Composite textComposite, String text) {
		Composite paragrafComposite = new Composite(CompositeType.PARAGRAPH);
		Pattern pattern = Pattern.compile(CompositeType.PARAGRAPH_WITH_LISTING.getExpression());
		Matcher matcher = pattern.matcher(text);
		String textPart = null;
		while (matcher.find()) {
			textPart = matcher.group();
			if (Pattern.matches(CompositeType.LISTING.getExpression(), textPart)) {
				Listing listing = new Listing(textPart);
				paragrafComposite.addComponent(listing);
				logger.info("Component - listing has been created");
			} else {
				paragrafComposite = parseToSentense(paragrafComposite, textPart);
				logger.info("Component - paragraf has been created");
			}

		}
		textComposite.addComponent(paragrafComposite);
		return textComposite;
	}

	private Composite parseToSentense(Composite paragrafComposite, String textPart) {
		Composite sentenseComposite = new Composite(CompositeType.SENTENCE);
		Pattern pattern = Pattern.compile(CompositeType.SENTENCE.getExpression());
		Matcher matcher = pattern.matcher(textPart);
		String sentense = null;
		while (matcher.find()) {
			sentense = matcher.group();
			sentenseComposite = parseToWordSign(sentenseComposite, sentense);
			logger.info("Component - sentence has been created");
		}
		paragrafComposite.addComponent(sentenseComposite);
		return paragrafComposite;
	}

	private Composite parseToWordSign(Composite sentenseComposite, String textPart) {
		Composite wordComposite = new Composite(CompositeType.WORD);
		Pattern pattern = Pattern.compile(CompositeType.WORD_AND_SIGN.getExpression());
		Matcher matcher = pattern.matcher(textPart);
		String word = null;
		while (matcher.find()) {
			word = matcher.group();
			if (Pattern.matches(CompositeType.WORD.getExpression(), word)) {
				wordComposite = parseToSymbol(wordComposite, word);
				logger.info("Component - word has been created");
			} else {
				Leaf leaf = new Leaf(word.charAt(0));
				wordComposite.addComponent(leaf);
				logger.info("Component - special symbol has been created");
			}
		}
		sentenseComposite.addComponent(wordComposite);
		return sentenseComposite;
	}

	private Composite parseToSymbol(Composite wordComposite, String textPart) {
		Composite symbolComposite = new Composite(CompositeType.SYMBOL);
		Pattern pattern = Pattern.compile(CompositeType.SYMBOL.getExpression());
		Matcher matcher = pattern.matcher(textPart);
		String symbol = null;
		while (matcher.find()) {
			symbol = matcher.group();
			Leaf leaf = new Leaf(symbol.charAt(0));
			symbolComposite.addComponent(leaf);
			logger.info("Component - char sympol has been created");
		}
		wordComposite.addComponent(symbolComposite);
		return wordComposite;
	}

}
