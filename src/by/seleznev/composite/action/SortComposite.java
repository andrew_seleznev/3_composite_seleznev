package by.seleznev.composite.action;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import by.seleznev.composite.entity.Composite;
import by.seleznev.composite.report.ReportComposite;

public class SortComposite {

	private static final HashSet<Character> VOWELS = new HashSet<Character>(
			Arrays.asList(new Character[] { 'a', 'e', 'i', 'o', 'u', 'y' }));
	private static final int UNICODE_SMALL_A = 97;
	private static final int UNICODE_SMALL_Z = 122;

	public static void sortWordsByConsAlph(Composite composite) {
		ArrayList<String> list = SearchComposite.searchAllWords(composite);
		ArrayList<String> listFirstVowelWord = new ArrayList<>();
		ArrayList<String> removeList = new ArrayList<>();

		list.stream().forEach((word) -> {
			if (word.length() > 1 && VOWELS.contains(word.charAt(0))) {
				listFirstVowelWord.add(word);
			}
		});

		removeList = (ArrayList<String>) listFirstVowelWord.stream().filter((word) -> {
			for (int i = 0; i < word.length(); i++) {
				Character ch = word.toLowerCase().charAt(i);
				if (!VOWELS.contains(ch) && word.toLowerCase().codePointAt(i) >= UNICODE_SMALL_A
						&& word.toLowerCase().codePointAt(i) <= UNICODE_SMALL_Z) {
					return false;
				}
			}
			return true;
		}).collect(Collectors.toList());

		listFirstVowelWord.removeAll(removeList);

		listFirstVowelWord.sort((s1, s2) -> {
			int c1 = 0;
			int c2 = 0;
			for (int i = 0; i < s1.length(); i++) {
				Character ch = s1.toLowerCase().charAt(i);
				if (!VOWELS.contains(ch) && s1.toLowerCase().codePointAt(i) >= UNICODE_SMALL_A
						&& s1.toLowerCase().codePointAt(i) <= UNICODE_SMALL_Z) {
					c1 = s1.toLowerCase().codePointAt(i);
					break;
				}
			}

			for (int i = 0; i < s2.length(); i++) {
				Character ch = s2.toLowerCase().charAt(i);
				if (!VOWELS.contains(ch) && s2.toLowerCase().codePointAt(i) >= UNICODE_SMALL_A
						&& s2.toLowerCase().codePointAt(i) <= UNICODE_SMALL_Z) {
					c2 = s2.toLowerCase().codePointAt(i);
					break;
				}
			}
			return Integer.compare(c1, c2);
		});
		ReportComposite.printSortedWordsByConsAlph(listFirstVowelWord);
	}

	public static void sortSentencesByWordAmount(Composite composite) {
		HashMap<String, Integer> map = SearchComposite.searchAllSentences(composite);
		List<String> sent = map.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue))
				.map(Map.Entry::getKey).collect(Collectors.toList());
		ReportComposite.printSortedSentences(sent);
	}
}
