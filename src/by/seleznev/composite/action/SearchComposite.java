package by.seleznev.composite.action;

import java.util.ArrayList;
import java.util.HashMap;
import by.seleznev.composite.entity.Component;
import by.seleznev.composite.entity.Composite;
import by.seleznev.composite.enumeration.CompositeType;

public class SearchComposite {
	
	private static final String SPACE_SYMBOL = "[\\s]";
	private static final String REPLACE_SYMBOL = " ";

	public static ArrayList<String> searchAllWords(Composite textComposite) {
		ArrayList<String> list = new ArrayList<>();
		for (Component component : textComposite.getComponents()) {
			if (component.getClass().equals(Composite.class)) {
				Composite composite = (Composite) component;
				if (composite.getCompositeType().name().equals(CompositeType.WORD.name())) {
					for (Component comp : composite.getComponents()) {
						if (comp.getClass().equals(Composite.class)) {
							list.add(comp.buildText());
						}
					}

				}
				list.addAll(searchAllWords(composite));
			}
		}
		return list;
	}

	public static HashMap<String, Integer> searchAllSentences(Composite textComposite) {
		HashMap<String, Integer> map = new HashMap<>();
		for (Component component : textComposite.getComponents()) {
			if (component.getClass().equals(Composite.class)) {
				Composite composite = (Composite) component;
				if (composite.getCompositeType().name().equals(CompositeType.SENTENCE.name())) {
					for (Component comp : composite.getComponents()) {
						if (comp.getClass().equals(Composite.class)) {
							Composite c = (Composite) comp;
							int count = 0;
							for (Component word : c.getComponents()) {
								if (word.getClass().equals(Composite.class)) {
									count++;
								}
							}
							map.put(comp.buildText().replaceAll(SPACE_SYMBOL, REPLACE_SYMBOL), count);
						}
					}
				}
				map.putAll(searchAllSentences(composite));
			}
		}
		return map;
	}

}
