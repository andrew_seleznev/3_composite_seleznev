package by.seleznev.composite.action;

import java.util.Iterator;

import by.seleznev.composite.entity.Component;
import by.seleznev.composite.entity.Composite;
import by.seleznev.composite.enumeration.CompositeType;

public class CountComposite {
	
	public static int counAllChartSymbols(Composite textComposite) {
		int i = 0;
		for (Component component : textComposite.getComponents()) {
			if (component.getClass().equals(Composite.class)) {
				Composite composite = (Composite) component;
				if (composite.getCompositeType().name().equals(CompositeType.SYMBOL.name())) {
					Iterator<Component> iterator = composite.getComponents().iterator();
					while (iterator.hasNext()) {
						iterator.next();
						i++;
					}
				}
				i += counAllChartSymbols(composite);
			}
		}
		return i;
	}
}
