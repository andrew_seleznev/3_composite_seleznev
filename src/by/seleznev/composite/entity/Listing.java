package by.seleznev.composite.entity;

public class Listing implements Component {
	
	private String code;
	
	public Listing(String code) {
		this.code = code;
	}

	@Override
	public String buildText() {
		return code;
	}

}
