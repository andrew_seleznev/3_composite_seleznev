package by.seleznev.composite.entity;

import java.util.ArrayList;

import by.seleznev.composite.enumeration.CompositeType;

public class Composite implements Component {

	private ArrayList<Component> components = new ArrayList<>();
	private CompositeType compositeType;

	public Composite(CompositeType textPartType) {
		this.compositeType = textPartType;
	}

	public void addComponent(Component component) {
		components.add(component);
	}

	public void removeComponent(Component component) {
		components.remove(component);
	}

	public CompositeType getCompositeType() {
		return compositeType;
	}

	public ArrayList<Component> getComponents() {
		return components;
	}

	@Override
	public String buildText() {
		StringBuilder sb = new StringBuilder();
		for (Component component : components) {
			sb.append(component.buildText());
		}
		return sb.toString();
	}

}
