package by.seleznev.composite.entity;

public class Leaf implements Component {

	private char symbol;

	public Leaf(char symbol) {
		this.symbol = symbol;
	}

	@Override
	public String buildText() {
		return String.valueOf(symbol);
	}

}
