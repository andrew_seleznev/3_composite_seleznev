package by.seleznev.composite.entity;

public interface Component {
	
	public String buildText();
	
}
